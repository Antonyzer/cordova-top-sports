const divListTop = '<li class="list-group-item" onclick="showOneTop(\'__slug__\')">Top __name__</li>';
const divOneTop = `<div class="card m-auto" style="width: 18rem;">
  <img class="card-img-top m-auto w-50" src="img/tops/__img__" alt="__name__">
  <div class="card-body">
    <p class="card-text">__number__ - __name__ - <a onclick="openInAppBrowser('__link__')">Voir</a></p>
  </div>
</div>`;

const htmlToElement = (html) => {
  const template = document.createElement("template");
  html = html.trim(); // Never return a text node of whitespace as the result
  template.innerHTML = html;
  return template.content.firstChild;
};

const fetchLocal = (url) => {
    return new Promise(function (resolve, reject) {
        const xhr = new XMLHttpRequest();
        xhr.onload = function () {
        resolve(new Response(xhr.response, { status: xhr.status }));
        };
        xhr.onerror = function () {
        reject(new TypeError("Local request failed"));
        };
        xhr.open("GET", url);
        xhr.responseType = "arraybuffer";
        xhr.send(null);
    });
};

const showListTop = () => {
    // Hide others page parts
    document.getElementById('createTopForm').classList.add('d-none');
    document.getElementById('oneTop').classList.add('d-none');

    // Get div in HTML and get tops from localstorage
    const divList = document.getElementById('listTops');
    const tops = JSON.parse(window.localStorage.getItem('tops'));
    
    // Reset list
    divList.innerHTML = '';
    // Create one li element for each top name
    tops.forEach((top) => {
        const newDivListTop = divListTop
            .replace('__slug__', top.slug)
            .replace('__name__', top.name);
        divList.appendChild(htmlToElement(newDivListTop));
    });
    // Display home part
    document.getElementById('listTopsDiv').classList.remove('d-none');
    document.getElementById('btnCreateTopForm').classList.remove('d-none');
}

const showOneTop = (slug) => {
    // Hide home part
    document.getElementById('listTopsDiv').classList.add('d-none');
    document.getElementById('btnCreateTopForm').classList.add('d-none');

    // Find selected top in local storage
    const top = JSON.parse(window.localStorage.getItem('tops')).find(
        (top) => top.slug === slug 
    );

    // Reset current top list
    document.getElementById('oneTopName').innerHTML = top.name;
    const divOneTopList = document.getElementById('oneTopList');
    divOneTopList.innerHTML = '';
    let number = 1;
    // Display elements of top list
    top.list.forEach((el) => {
        const newDivOneTop = divOneTop
            .replace('__img__', el.img)
            .replace('__number__', number)
            .replace(/__name__/g, el.name)
            .replace('__link__', el.link);
        divOneTopList.appendChild(htmlToElement(newDivOneTop));
        number++;
    });

    // Show one top
    document.getElementById('oneTop').classList.remove('d-none');
}

const saveApiDataInLocalstorage = (json) => {
    return new Promise((resolve, reject) => {
        window.localStorage.setItem('tops', JSON.stringify(json));
        resolve('success');
    });
}

const fetchApiTops = () => {
    if (!window.localStorage.getItem('tops')) {
        fetchLocal('api/tops.json')
            .then((response) => {
                response.json().then((json) => {
                    // Save tops in localstorage
                    saveApiDataInLocalstorage(json)
                        .then(() => {
                            // Display list of tops
                            showListTop();
                        });
                });
            })
    } else {
        // Display list of tops
        showListTop();
    }
}

if ("cordova" in window) {
    document.addEventListener("deviceready", fetchApiTops);
} else {
    document.addEventListener("DOMContentLoaded", fetchApiTops);
}