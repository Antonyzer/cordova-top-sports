const eleForm = `<div>
<h2>__number__.</h2>
<input type="hidden" name="top[__number__][number]" value="__number__">
<div class="form-group">
    <label for="name__number__">Intitulé</label>
    <input type="text" name="top[__number__][name]" id="name__number__" class="form-control" required>
</div>
<div class="form-group">
    <label for="link__number__">Lien</label>
    <input type="text" name="top[__number__][link]" id="link__number__" class="form-control">
</div>
<!--
<div class="form-group">
    <label for="img__number__">Image</label>
    <input type="file" name="top[__number__][img]" id="img__number__" class="form-control" required>
</div>
-->
</div>`;
let eleNumberForm = 1;

const showCreateForm = () => {
    // Hide list and one top
    document.getElementById('listTopsDiv').classList.add('d-none');
    document.getElementById('oneTop').classList.add('d-none');
    document.getElementById('btnCreateTopForm').classList.add('d-none');
    // Reset input
    document.getElementById('topName').value = '';
    // Display form
    eleNumberForm = 1;
    document.getElementById('topListForm').innerHTML = '';
    document.getElementById('createTopForm').classList.remove('d-none');
}

const createEmptyTopElement = () => {
    const topListForm = document.getElementById('topListForm');
    // Generate group form for new element
    const newEleForm = eleForm
        .replace(/__number__/g, eleNumberForm);
    topListForm.appendChild(htmlToElement(newEleForm));
    // Increment number for next potential element
    eleNumberForm++;
}

const saveTopForm = (e) => {
    e.preventDefault();

    let number = 0;

    // Get data from form
    const topName = document.getElementById('topName').value;
    let topSlug = topName.toLowerCase().replace(/[" "]/g, '-');

    // Check slug not already exists
    const oldTop = JSON.parse(localStorage.getItem('tops')).find(
        (top) => top.slug === topSlug 
    );

    if (oldTop) {
        topSlug = 'new-' + topSlug;
    }

    const inputs = document.getElementById('topForm').elements;

    const newTop = {
        "slug": topSlug,
        "name": topName,
        "list": []
    };

    for (let i = 1; i < inputs.length; i++) {
        if (inputs[i].type == 'hidden') {
            number = inputs[i].value;
            newTop.list[number - 1] = {
                "name": "",
                "link": "",
                "img": "default.jpg"
            };
        } else if (inputs[i].type == 'text') {
            if (inputs[i].name.endsWith('top[' + number + '][name]')) {
                newTop.list[number - 1].name = inputs[i].value;
            } else if (inputs[i].name.endsWith('top[' + number + '][link]')) {
                newTop.list[number - 1].link = inputs[i].value;
            }
        }
    }

    // Save data in localstorage
    const tops = JSON.parse(localStorage.getItem('tops'));
    tops.push(newTop);
    localStorage.setItem('tops', JSON.stringify(tops));

    // Display home part
    showListTop();
}

const cancelTopForm = () => {
    // Display home part
    showListTop();
}